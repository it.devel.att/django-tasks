from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from tasks.factories.task import TaskFactory
from tasks.factories.user import UserFactory
from tasks.models import Task


class TaskDeleteViewTestCase(APITestCase):

    def setUp(self) -> None:
        super().setUp()
        self.user = UserFactory()
        refresh = RefreshToken.for_user(self.user)
        self.auth = {'HTTP_AUTHORIZATION': f'Bearer {refresh.access_token}'}
        self.task = TaskFactory(author=self.user, executors=UserFactory.create_batch(10))
        self.url = reverse('tasks:update_delete_retrieve', args=(self.task.id,))

    def test_cannot_delete_without_auth(self):
        response = self.client.delete(self.url)
        self.assertEqual(401, response.status_code)

    def test_cannot_delete_not_self_author_task(self):
        user = UserFactory()
        refresh = RefreshToken.for_user(user)
        auth = {'HTTP_AUTHORIZATION': f'Bearer {refresh.access_token}'}

        response = self.client.delete(self.url, **auth)
        self.assertEqual(403, response.status_code)

    def test_can_delete_task(self):
        response = self.client.delete(self.url, **self.auth)
        self.assertEqual(204, response.status_code)
        self.assertIsNone(Task.objects.filter(pk=self.task.id).first())
