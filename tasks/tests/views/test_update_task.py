import json
import os

from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from tasks.factories.task import TaskFactory
from tasks.factories.user import UserFactory
from tasks.models import Task

CURRENT_DIR = os.path.dirname(__file__)
RESOURCES_DIR = os.path.join(CURRENT_DIR, 'resources')


class TaskUpdateViewTestCase(APITestCase):

    def setUp(self) -> None:
        super().setUp()
        self.user = UserFactory()
        refresh = RefreshToken.for_user(self.user)
        self.auth = {'HTTP_AUTHORIZATION': f'Bearer {refresh.access_token}'}
        self.task = TaskFactory(author=self.user, executors=UserFactory.create_batch(10))
        self.url = reverse('tasks:update_delete_retrieve', args=(self.task.id,))

    def get_default_body(self) -> dict:
        return {
            'title': 'Updated Title',
            'description': 'Updated description',
        }

    def test_cannot_update_without_auth(self):
        data = self.get_default_body()

        response = self.client.patch(self.url, data=data)
        self.assertEqual(401, response.status_code)

    def test_cannot_update_not_self_author_task(self):
        data = self.get_default_body()
        user = UserFactory()
        refresh = RefreshToken.for_user(user)
        auth = {'HTTP_AUTHORIZATION': f'Bearer {refresh.access_token}'}

        response = self.client.patch(self.url, data=data, **auth)
        self.assertEqual(403, response.status_code)

    def test_update_task_in_db(self):
        data = self.get_default_body()

        response = self.client.patch(
            self.url,
            data=json.dumps(data),
            **self.auth,
            content_type='application/json',
        )
        self.assertEqual(200, response.status_code)

        last_task = Task.objects.get(pk=self.task.id)
        self.assertEqual(data['title'], last_task.title)
        self.assertEqual(data['description'], last_task.description)
        self.assertEqual(self.user.id, last_task.author_id)
        self.assertEqual(10, last_task.executors.count())

    def test_update_task_response_data_with_minimum_required_fields(self):
        data = self.get_default_body()
        response = self.client.patch(
            self.url,
            data=json.dumps(data),
            **self.auth,
            content_type='application/json',
        )
        self.assertEqual(200, response.status_code)

        response_body = response.json()
        self.assertEqual(data['title'], response_body['title'])
        self.assertEqual(data['description'], response_body['description'])
        self.assertTrue(self.task.image.name in response_body['image'])
        self.assertEqual(10, len(response_body['executors']))
        self.assertEqual(
            {
                'id': self.user.id,
                'first_name': self.user.first_name,
                'last_name': self.user.last_name,
                'email': self.user.email,
            },
            response_body['author']
        )

    def test_update_task_with_executors(self):
        data = self.get_default_body()

        executors = UserFactory.create_batch(5)
        data['executors'] = [user.id for user in executors]
        response = self.client.patch(
            self.url,
            data=json.dumps(data), **self.auth,
            content_type='application/json',
        )
        self.assertEqual(200, response.status_code)

        response_body = response.json()
        self.assertEqual(data['title'], response_body['title'])
        self.assertEqual(data['description'], response_body['description'])
        self.assertTrue(self.task.image.name in response_body['image'])
        self.assertEqual(
            {
                'id': self.user.id,
                'first_name': self.user.first_name,
                'last_name': self.user.last_name,
                'email': self.user.email,
            },
            response_body['author']
        )
        self.assertEqual(5, len(response_body['executors']))
        for executor in executors:
            self.assertTrue(
                {
                    'id': executor.id,
                    'first_name': executor.first_name,
                    'last_name': executor.last_name,
                    'email': executor.email,
                } in response_body['executors']
            )

    def test_update_task_with_executors_and_image(self):
        data = self.get_default_body()

        executors = UserFactory.create_batch(5)
        data['executors'] = [user.id for user in executors]
        with open(os.path.join(RESOURCES_DIR, 'golang.jpeg'), 'rb') as fp:
            data['image'] = fp

            response = self.client.patch(
                self.url,
                data=data,
                **self.auth,
                format='multipart',
            )
        self.assertEqual(200, response.status_code)
        response_body = response.json()
        self.assertEqual(data['title'], response_body['title'])
        self.assertEqual(data['description'], response_body['description'])
        self.assertTrue('golang' in response_body['image'])
        self.assertEqual(
            {
                'id': self.user.id,
                'first_name': self.user.first_name,
                'last_name': self.user.last_name,
                'email': self.user.email,
            },
            response_body['author']
        )
        self.assertEqual(5, len(response_body['executors']))
        for executor in executors:
            self.assertTrue(
                {
                    'id': executor.id,
                    'first_name': executor.first_name,
                    'last_name': executor.last_name,
                    'email': executor.email,
                } in response_body['executors']
            )
