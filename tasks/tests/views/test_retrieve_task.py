from django.urls import reverse
from rest_framework.test import APITestCase

from tasks.factories.task import TaskFactory
from tasks.factories.user import UserFactory


class TaskRetrieveViewTestCase(APITestCase):

    def setUp(self) -> None:
        super().setUp()
        self.user = UserFactory()
        self.task = TaskFactory(author=self.user, executors=UserFactory.create_batch(10))
        self.url = reverse('tasks:update_delete_retrieve', args=(self.task.id,))

    def test_can_get_any_task_without_auth(self):
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)
        response_data = response.json()
        self.assertEqual(self.task.id, response_data['id'])
        self.assertEqual(self.task.title, response_data['title'])
        self.assertEqual(self.task.description, response_data['description'])
        self.assertTrue(self.task.image.name in response_data['image'])
