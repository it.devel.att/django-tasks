from django.urls import reverse
from rest_framework.test import APITestCase

from tasks.factories.task import TaskFactory
from tasks.factories.user import UserFactory


class TaskListViewTestCase(APITestCase):

    def test_get_list_of_tasks(self):
        TaskFactory.create_batch(20)
        response = self.client.get(reverse('tasks:list_create'))
        self.assertEqual(200, response.status_code)

        response_body = response.json()
        self.assertEqual(20, response_body['count'])
        self.assertEqual(20, len(response_body['results']))
        self.assertIsNone(response_body['next'])
        self.assertIsNone(response_body['previous'])

    def test_check_data_of_list(self):
        executor = UserFactory()
        task = TaskFactory(executors=[executor])
        response = self.client.get(reverse('tasks:list_create'))
        self.assertEqual(200, response.status_code)

        response_body = response.json()['results'][0]
        self.assertEqual(task.title, response_body['title'])
        self.assertEqual(task.description, response_body['description'])
        self.assertTrue(task.image.name in response_body['image'], task.image.name)
        self.assertEqual(
            {
                'id': task.author.id,
                'first_name': task.author.first_name,
                'last_name': task.author.last_name,
                'email': task.author.email,
            },
            response_body['author']
        )
        self.assertEqual(1, len(response_body['executors']))
        self.assertEqual(
            {
                'id': executor.id,
                'first_name': executor.first_name,
                'last_name': executor.last_name,
                'email': executor.email,
            },
            response_body['executors'][0]
        )
