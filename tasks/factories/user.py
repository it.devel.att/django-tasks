import factory
from django.contrib.auth import get_user_model
from factory import Sequence


class UserFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = get_user_model()

    username = Sequence(lambda i: f'User Name {i}')
    first_name = Sequence(lambda i: f'John {i}')
    last_name = Sequence(lambda i: f'Doe {i}')
    email = Sequence(lambda i: f'some_email_{i}@email.com')
    password = factory.PostGenerationMethodCall('set_password', '123455')



