import factory
from factory import Sequence, SubFactory

from tasks.factories.user import UserFactory
from tasks.models import Task


class TaskFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Task

    title = Sequence(lambda i: f'Title {i}')
    description = Sequence(lambda i: f'Description {i}')
    author = SubFactory(UserFactory)
    image = factory.django.ImageField()

    @factory.post_generation
    def executors(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for executor in extracted:
                self.executors.add(executor)
