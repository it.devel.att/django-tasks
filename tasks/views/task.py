from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from tasks.auth.task import IsTaskOwner
from tasks.models import Task
from tasks.serializers.task import TaskSerializer, TaskCreateSerializer, TaskUpdateSerializer


class TaskListCreateView(generics.ListAPIView, generics.CreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    create_serializer_class = TaskCreateSerializer
    parser_classes = (
        MultiPartParser,
        FormParser,
        JSONParser,
    )

    def check_permissions(self, request):
        if request.method == 'POST':
            super().check_permissions(request)

    @swagger_auto_schema(responses={200: TaskSerializer(many=True)})
    def get(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(responses={201: serializer_class}, request_body=create_serializer_class)
    def post(self, request, *args, **kwargs):
        serializer = self.create_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        task = serializer.save(author=self.request.user)
        show_serializer = self.serializer_class(instance=task)
        return Response(show_serializer.data, status=status.HTTP_201_CREATED)


class TaskUpdateDestroyRetrieveView(generics.UpdateAPIView, generics.DestroyAPIView, generics.RetrieveAPIView):
    queryset = Task.objects.all()
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated, IsTaskOwner]
    serializer_class = TaskSerializer
    update_serializer_class = TaskUpdateSerializer
    parser_classes = (
        MultiPartParser,
        FormParser,
        JSONParser,
    )

    def check_permissions(self, request):
        if request.method != 'GET':
            super().check_permissions(request)

    def check_object_permissions(self, request, obj):
        if request.method != 'GET':
            super().check_object_permissions(request, obj)

    @swagger_auto_schema(responses={200: serializer_class}, request_body=update_serializer_class)
    def patch(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.update_serializer_class(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        task = serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(self.serializer_class(instance=task).data)
