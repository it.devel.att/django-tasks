from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from common.models import TimestampAbstractModel


class Task(TimestampAbstractModel):
    title = models.CharField(
        verbose_name=_('title'),
        max_length=255,
    )
    description = models.TextField(
        verbose_name=_('description'),
    )
    author = models.ForeignKey(
        verbose_name=_('author'),
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='tasks',
    )
    image = models.ImageField(
        verbose_name=_('image'),
        upload_to='tasks/images',
        blank=True,
        null=True,
    )
    executors = models.ManyToManyField(
        verbose_name=_('executors'),
        to=settings.AUTH_USER_MODEL,
        related_name='tasks_to_perform',
        blank=True,
    )

    class Meta:
        verbose_name = _('task')
        verbose_name_plural = _('tasks')
