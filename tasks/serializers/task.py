from django.conf import settings
from django.core.mail import send_mail
from rest_framework.serializers import ModelSerializer

from tasks.models import Task
from tasks.serializers.user import UserSerializer


class TaskSerializer(ModelSerializer):
    author = UserSerializer()
    executors = UserSerializer(many=True)

    class Meta:
        model = Task
        fields = (
            'id',
            'title',
            'description',
            'author',
            'image',
            'executors',
        )


class TaskCreateSerializer(ModelSerializer):
    class Meta:
        model = Task
        fields = (
            'title',
            'description',
            'image',
            'executors',
        )

    def create(self, validated_data):
        executors = validated_data.pop('executors', None)
        task = super().create(validated_data)
        if executors:
            task.executors.set(executors)
            # TODO it's a very simple logic, better to use celery for make this execution async
            # Also it's better to put some link here to task. But link depends on frontend url and not backend
            send_mail(f"Task {task.title} is assign on you! Do something ;D",
                      f"On you assign task {task.title}",
                      settings.EMAIL_HOST_USER,
                      [executor.email for executor in executors],
                      fail_silently=True)
        return task

    def save(self, author):
        self.validated_data.update(author=author)
        return self.create(self.validated_data)


class TaskUpdateSerializer(ModelSerializer):
    class Meta:
        model = Task
        fields = (
            'title',
            'description',
            'image',
            'executors',
        )

    def update(self, instance, validated_data):
        already_executors = list(instance.executors.values_list('email', flat=True))
        executors = validated_data.pop('executors', None)
        task = super().update(instance, validated_data)
        if executors:
            task.executors.set(executors)

            executors_email = list(task.executors.values_list('email', flat=True))
            new_executers = list(set(executors_email) - set(already_executors))
            deleted_executers = list(set(already_executors) - set(executors_email))

            if new_executers:
                # TODO it's a very simple logic, better to use celery for make this execution async
                send_mail(f"Task {task.title} is assign on you! Do something ;D",
                          f"On you assign task {task.title}",
                          settings.EMAIL_HOST_USER,
                          new_executers,
                          fail_silently=True)
            if deleted_executers:
                # TODO it's a very simple logic, better to use celery for make this execution async
                send_mail(f"Task {task.title} was delete from your tasks",
                          f"Task {task.title} deleted for you",
                          settings.EMAIL_HOST_USER,
                          deleted_executers,
                          fail_silently=True)
        return task
