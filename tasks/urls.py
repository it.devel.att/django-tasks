from django.urls import path

from tasks.views.task import TaskListCreateView, TaskUpdateDestroyRetrieveView

app_name = 'tasks'
urlpatterns = [
    path('tasks/', TaskListCreateView.as_view(), name='list_create'),
    path('tasks/<int:pk>', TaskUpdateDestroyRetrieveView.as_view(), name='update_delete_retrieve'),
]
